# wordle

![Release](https://img.shields.io/badge/Release-0.2-blue?labelColor=grey&style=flat)
![Node](https://img.shields.io/badge/Node-18.0.0+-blue?labelColor=grey&style=flat)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)

A WORDLE clone written in SvelteKit. 

[Play it here!](https://wordle-red.vercel.app/)

<img src="screenshot.png" width="320" height="500" />

## Getting started

```
npm install
npm run dev
```

## About
My wife seriously loves WORDLE. She has been playing the offical NYT version but one game per day is not enough to feed the addiction. So, I wrote this one for her and made it unlimited!
