import { browser } from '$app/environment';
import { get, writable } from 'svelte/store'

// general purpose Svelte store persisting in localStorage
export function storable(data, key = 'storable') {
    const store = writable(data);
    const { subscribe, set, update } = store;

    browser &&
        localStorage[key] &&
        set(JSON.parse(localStorage[key]));

    return {
        subscribe,
        set: n => {
            browser && (localStorage[key] = JSON.stringify(n));
            set(n);
        },
        update: cb => {
            const updatedStore = cb(get(store));

            browser && (localStorage[key] = JSON.stringify(updatedStore));
            set(updatedStore);
        }
    };
}