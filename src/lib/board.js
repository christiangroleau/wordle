// maximum number of rows and columns
export const ROWS = 6;
export const COLS = 5;

export const emptyBoard = () => {
	return Array.from({ length: ROWS }, () =>
		Array.from({ length: COLS }, () => ({ state: 'empty', letter: '' })));
}

// a board is a two-dimensional array of rows and columns like:
// [[ {state: 'present', letter: 's'}, {state: 'absent', letter: 't'}], ...]
class GameBoard {
	currRow = 0;
	currCol = 0;

	board = emptyBoard();

	word() {
		if (this.board[this.currRow]) {
			return this.board[this.currRow].map((col) => col.letter).join('');
		}
		return '';
	}

	addLetter(letter) {
		// are all rows or current row filled?
		if (this.currRow === ROWS || this.currCol === COLS) {
			return this.board;
		}

		this.board[this.currRow][this.currCol].state = 'active';
		this.board[this.currRow][this.currCol].letter = letter;

		// move pointer forward one column
		this.currCol = Math.min(this.currCol + 1, COLS);

		return this.board;
	}

	removeLetter() {
		// move pointer back one column
		this.currCol = Math.max(this.currCol - 1, 0);

		this.board[this.currRow][this.currCol].state = 'empty';
		this.board[this.currRow][this.currCol].letter = '';

		return this.board;
	}

	updateRow(statuses) {
		this.board[this.currRow] = statuses;

		// move to next row, leftmost column
		this.currRow = Math.min(this.currRow + 1, ROWS);
		this.currCol = 0;

		return this.board;
	}

	full() {
		return this.currRow === ROWS;
	}

	clear() {
		this.board = emptyBoard();
		this.currRow = this.currCol = 0;

		return this.board;
	}
};

export default GameBoard;
