import { storable } from '../utils/storable';

const initialValue = { started: false, active: false, over: false, win: false, secret: '' };

export const game = storable(initialValue, 'game');

export function startGame() {
	game.update($state => {
		$state.started = true;

		return $state;
	});
}

export function activateGame(secret) {
	game.update($state => {
		$state.started = false;
		$state.active = true;
		$state.over = false;
		$state.win = false;
		$state.secret = secret;

		return $state;
	});
}

export function endGame(win) {
	game.update($state => {
		$state.started = false;
		$state.active = false;
		$state.over = true;
		$state.win = win;
		
		return $state;
	});
}

