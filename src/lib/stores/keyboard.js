import { derived } from 'svelte/store';
import { board } from '$lib/stores/board';

export const selectedKeys = derived(board, $board => {
    let selected = new Map();

    $board.board.forEach((row) => {
        row.forEach((col) => {
            if (col.letter && !selected.get(col.letter)) {
                selected.set(col.letter, col.state);
            }
        });
    });

    return selected;
})