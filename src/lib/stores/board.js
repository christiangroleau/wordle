import { storable } from '$lib/utils/storable';
import { emptyBoard } from '$lib/board';

const initialValue = {
    board: emptyBoard(),
    row: 0
}
export const board = storable(initialValue, 'board');

export function saveBoard(b, r) {
    board.update(($state) => {
        $state.board = b;
        $state.row = r;

        return $state;
    });
}

export function clearBoard() {
    board.update($state => {
        $state.board = emptyBoard();
        $state.row = 0;

        return $state;
    });
}