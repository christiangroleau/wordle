import { storable } from '../utils/storable';

const initialValue = {
	// total number of games played
	games: 0,

	// total number of wins
	wins: 0,

	// total number of losses
	losses: 0,

	// ratio of wins
	winRatio: 0,

	// current winning streak
	currentStreak: 0,

	// highest historial winning streak
	maxStreak: 0,

	// count of guesses made in the current game
	guesses: 0,

	// historical count of guesses made to solve the game
	guessDistribution: { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0 }
};

export const statistics = storable(initialValue, 'statistics');

// initialize stats at the start of a game
export function initializeStats() {
	statistics.update(($state) => {
		$state.guesses = 0;

		return $state;
	});
}

// add a stat as they happen
export function addStat(stats) {
	statistics.update(($state) => {
		$state.guesses += 'guesses' in stats ? 1 : 0;

		return $state;
	});
}

// tally and finalize stats at the end of a game
export function completeStats(win) {
	statistics.update(($state) => {
		if (win) {
			$state.wins += 1;
			$state.guessDistribution[$state.guesses] += 1;
			$state.currentStreak += 1;
			$state.maxStreak = Math.max($state.currentStreak, $state.maxStreak);
		} else {
			$state.currentStreak = 0;
			$state.losses += 1;
		}

		$state.games += 1;
		$state.winRatio = Math.ceil(100 * ($state.wins / $state.games));

		return $state;
	});
}

