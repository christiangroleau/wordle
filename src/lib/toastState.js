export default class ToastState {
    message = '';
    ttl = 0;

    constructor() {
        this.message = '';
        this.ttl = 0;
    }

    update(message, ttl) {
        this.message = message;
        this.ttl = ttl ? ttl : 0;

        return this;
    }

    clear() {
        this.message = '';
        this.ttl = 0;

        return this;
    }
}