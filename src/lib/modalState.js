export default class ModalSate {
    type = '';
    delay = 0;

    constructor() {
        this.type = '';
        this.delay = 0;
    }

    update(type, delay) {
        this.type = type;
        this.delay = delay ? delay : 0;

        return this;
    }

    clear() {
        this.type = '';
        this.delay = 0;

        return this;
    }
}