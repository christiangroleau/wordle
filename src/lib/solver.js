import { COLS } from '$lib/board';

export class Solver {
	
	constructor(dictionary) {
		this.dictionary = dictionary;
	}

	validateGuess(guess) {
		let valid = true;
		let message = '';

		// is the guess at least 5 letters?
		if (guess.length !== COLS) {
			valid = false;
			message = `Not enough letters`;
		}

		// does the guess exist in the game's dictionary?
		else if (!this.dictionary.includes(guess)) {
			valid = false;
			message = `Not found in word list`;
		}

		// something like { valid: false, guess: 'fly', 'Not enough letters' }
		return { valid, guess, message };
	}

	compareToSecret(guess, secret) {
		if (guess.length !== COLS && secret.length !== COLS) {
			return;
		}

		// position in the secret word
		let position = 0;

		// list of incorrect letters that may be considered 'present'
		let incorrect = Array.from({ length: COLS }, (_, i) =>
			secret[i] !== guess[i] ? secret[i] : null
		);

		// count correct letters
		let correct = 0;

		// check each letter of the guess
		let statuses = Array.from(guess).map((letter) => {
			let state = 'absent';

			// positional match with the secret's letter?
			if (secret[position] === letter) {
				state = 'correct';
				correct += 1;
			}

			// letter found in secret and known to be incorrect
			else if (secret.includes(letter) && incorrect.includes(letter)) {
				state = 'present';
				incorrect[incorrect.indexOf(letter)] = null;
			}

			position += 1;

			return { state, letter };
		});

		// something like { match: true, [{ state: 'correct', letter: 's' }, ...] }
		return { match: correct === COLS, statuses };
	}
};

export default Solver;
