export const specialKeys = {
	enter: 'enter',
	backspace: '←'
};

export const keys = [
	'q',
	'w',
	'e',
	'r',
	't',
	'y',
	'u',
	'i',
	'o',
	'p',

	'a',
	's',
	'd',
	'f',
	'g',
	'h',
	'j',
	'k',
	'l',

	specialKeys.enter,
	'z',
	'x',
	'c',
	'v',
	'b',
	'n',
	'm',
	specialKeys.backspace
];

export const spacerBefore = 'a';
export const spacerAfter = 'l';

// keyboard rows as they appear on-screen
export const rows = [keys.slice(0, 10), keys.slice(10, 19), keys.slice(19)];
